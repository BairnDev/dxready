FROM node:alpine
RUN npm i npm@latest -g

RUN apk add --update
RUN apk add --update ca-certificates openssl && update-ca-certificates

RUN npm install sfdx-cli --global

RUN apk add --no-cache git

RUN apk add openssh